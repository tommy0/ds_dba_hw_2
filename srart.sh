#!/bin/bash

hadoop jar /usr/local/Cellar/hadoop/3.1.1/libexec/share/hadoop/tools/lib/hadoop-*streaming*.jar \
-file ./mapper.py \
-mapper ./mapper.py \
-file ./reducer.py  \
-reducer./reducer.py  \
-input daily/logs \
-output daily/output
