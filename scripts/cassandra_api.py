from cassandra.cluster import Cluster, Session
from log_config import log

KEYSPACE = "metrics_space"
CASSANDRA_NODES = ['127.0.0.1']


def create_kayspace(session: Session, keyspace: str):
    """
    Create keyspace from cassandra
    :param session: Cassandra session
    :param keyspace: cassandra keyspace
    :return:
    """

    log.info("creating keyspace...")
    session.execute("""
            CREATE KEYSPACE IF NOT EXISTS %s
            WITH replication = { 'class': 'SimpleStrategy', 'replication_factor': '2' }
            """ % keyspace)


def create_table(session: Session):
    """
    Create table from keyspacs cassandra
    :param session: Cassandra session
    :return:
    """
    log.info("creating table...")
    session.execute(f"""
            CREATE TABLE IF NOT EXISTS metrics (
                id int,
                timest timestamp,
                vlaue_data double,
                PRIMARY KEY (id, timest)
            )
            """)


def get_result(session: Session) -> list:
    """
    get data from cassandra table
    :param session: Cassandra session
    :return:
    """
    future = session.execute_async("SELECT * FROM metrics")
    try:
        return future.result()
    except Exception as e:
        log.exception(f"Error reading rows: {e}")
        raise


def print_result(rows: list):
    """
    print result rows in log
    :param rows: result rows
    :return:
    """
    log.info("key\tcol1\tcol2")
    log.info("---\t----\t----")

    for row in rows:
        log.info(f'{row.id} \t {row.timest} \t {row.vlaue_data}')


def drop_keyspace(session: Session, keyspace: str):
    """
    Drop keysapsce from cassandra
    :param session: Cassandra session
    :param keyspace: cassandra keyspace
    :return:
    """
    session.execute("DROP KEYSPACE " + keyspace)


def main():
    """
    main script which create keyspace, create table, and select form table
    :return:
    """
    cluster = Cluster(CASSANDRA_NODES, port=9042)
    session = cluster.connect()
    create_kayspace(session, KEYSPACE)
    session.set_keyspace(KEYSPACE)
    create_table(session)
    rows = get_result(session)
    print_result(rows)


if __name__ == "__main__":
    main()
