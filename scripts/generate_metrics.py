import psutil
import os
from datetime import datetime
import time
import random
from typing import Generator

NUM_METRICS = 500000


class BaseMetric:
    """
    Base class for metic implementation
    :parameter metric_id: id metric
    :parameter name: name of metric
    """
    metric_id: int
    name: str

    def __init__(self, process: psutil.Process):
        """
        :param process: current process
        """
        self.process = process
        self.timestamp = int(datetime.timestamp(datetime.now()))
        self.value = self.get_value()

    def get_value(self):
        """
        Abstract method for each metrics implementation
        """
        raise NotImplemented

    def __repr__(self) -> str:
        return f'{self.metric_id}, {self.timestamp}, {self.value}'

    def __str__(self) -> str:
        return f'{self.metric_id}, {self.timestamp}, {self.value}'


class CpuPercentMetric(BaseMetric):
    """
    cpu_percent class for metic implementation
    :parameter metric_id: int
    :parameter name: str
    """
    metric_id = 1
    name = 'cpu_percent'

    def get_value(self) -> float:
        return self.process.cpu_percent()


class CpuTimesUserMetric(BaseMetric):
    """
    cpu_times_user for metic implementation
    :parameter metric_id: int
    :parameter name: str
    """
    metric_id = 2
    name = 'cpu_times_user'

    def get_value(self) -> float:
        return self.process.cpu_times().user


class CpuTimesSystemMetric(BaseMetric):
    """
    cpu_times_system class for metic implementation
    :parameter metric_id: int
    :parameter name: str
    """
    metric_id = 3
    name = 'cpu_times_system'

    def get_value(self) -> float:
        return self.process.cpu_times().system


class ThreadsMetric(BaseMetric):
    """
    threads_user_time class for metic implementation
    :parameter metric_id: int
    :parameter name: str
    """
    metric_id = 4
    name = 'threads_user_time'

    def get_value(self) -> float:
        return self.process.threads()[0].user_time


class MemoryVmsMetric(BaseMetric):
    """
    memory_vms for metic implementation
    :parameter metric_id: int
    :parameter name: str
    """
    metric_id = 5
    name = 'memory_vms'

    def get_value(self) -> float:
        return self.process.memory_full_info().vms


class MemoryRssMetric(BaseMetric):
    """
    memory_rss for metic implementation
    :parameter metric_id: int
    :parameter name: str
    """
    metric_id = 6
    name = 'memory_rss'

    def get_value(self) -> float:
        return self.process.memory_full_info().rss


class MemoryPfaultsMetric(BaseMetric):
    """
    memory_pfaults for metic implementation
    :parameter metric_id: int
    :parameter name: str
    """
    metric_id = 7
    name = 'memory_pfaults'

    def get_value(self) -> float:
        return self.process.memory_full_info().pfaults


class MemoryPageinsMetric(BaseMetric):
    """
    memory_pageins for metic implementation
    :parameter metric_id: int
    :parameter name: str
    """
    metric_id = 8
    name = 'memory_pageins'

    def get_value(self) -> float:
        return self.process.memory_full_info().pageins


class MemoryUssMetrc(BaseMetric):
    """
    memory_uss for metic implementation
    :parameter metric_id: int
    :parameter name: str
    """
    metric_id = 9
    name = 'memory_uss'

    def get_value(self) -> float:
        return self.process.memory_full_info().uss


# Metrics mappings
metrics_type = {
    'cpu_percent': CpuPercentMetric,
    'cpu_times_user': CpuTimesUserMetric,
    'cpu_times_system': CpuTimesSystemMetric,
    'threads': ThreadsMetric,
    'memory_vms': MemoryVmsMetric,
    'memory_rss': MemoryRssMetric,
    'memory_pfaults': MemoryPfaultsMetric,
    'memory_pageins': MemoryPageinsMetric,
    'memory_uss': MemoryUssMetrc,
}


def generate_metrics(process: psutil.Process) -> Generator[list]:
    """
    Snapshot matrics of current process state
    :param process: current process
    :return: Generator[list] - metrics for current timestamp
    """
    for i in range(NUM_METRICS):
        time_sleep = random.randint(1, 100) // 100
        time.sleep(time_sleep)
        metrics = []
        for key, value in metrics_type.items():
            print(f'{i} from {NUM_METRICS} generated')
            metrics.append(value(process))
        yield metrics


def write_metrics(filename: str):
    """
    Write metrics if filetest.py
    :param filename: name of file
    :return:
    """
    p = psutil.Process(os.getpid())
    with open(filename, 'w') as f:
        for metrics in generate_metrics(p):
            for metric in metrics:
                f.write(f'{metric}\n')


if __name__ == '__main__':
    write_metrics('metrics.txt')
