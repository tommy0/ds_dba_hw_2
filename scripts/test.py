import pytest # noqa
import psutil
import os
from .generate_metrics import generate_metrics
from .cassandra_api import create_table, create_kayspace, drop_keyspace, get_result, Cluster, CASSANDRA_NODES, KEYSPACE


def test_generate_metrics():
    """
    Test generate metrics
    :return:
    """
    p = psutil.Process(os.getpid())
    gm = generate_metrics(p)
    metrics = next(gm)

    assert len(metrics) == 9

    for metric in metrics:
        m = str(metric).split(',')
        assert len(m) == 3


def test_cassandra_api():
    """
    Test cassandra api
    :return:
    """
    keyspace = "Test"
    cluster = Cluster(CASSANDRA_NODES, port=9042)
    session = cluster.connect()
    create_kayspace(session, keyspace)
    session.set_keyspace(keyspace)
    create_table(session)
    rows = get_result(session)
    assert len(rows) == 0
    drop_keyspace(session, keyspace)
