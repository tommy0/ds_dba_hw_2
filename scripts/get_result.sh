#!/usr/bin/env bash

sqoop export --connect 'jdbc:datadirect:cassandra://127.0.0.1:9042;KeyspaceName=metrics_space' --driver com.ddtek.jdbc.cassandra.CassandraDriver --table 'metrics' --export-dir /user/tommyz/result --input-lines-terminated-by "n" --input-fields-terminated-by ',' --batch -m 1