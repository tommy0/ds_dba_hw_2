import scala.Tuple2;
import scala.Tuple3;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.SparkConf;

import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;

/**
 * Aggreagte metrics with spark
 */
public final class Main {

    public static void main(String[] args) throws Exception {
        SparkConf conf = new SparkConf().setAppName("metics").setMaster("local");
        JavaSparkContext sc = new JavaSparkContext(conf);
        JavaRDD<String> csvData = sc.textFile("scripts/metrics.txt");

        // Csv to rdd of tuple3
        JavaRDD<Tuple3<Integer, Integer, Double>> x = csvData.map(
                new Function<String, Tuple3<Integer, Integer, Double>>() {
                    @Override
                    public Tuple3<Integer, Integer, Double> call(String s) {
                        String[] values = s.split(", ");
                        int id = Integer.parseInt(values[0]);
                        int ts = Integer.parseInt(values[1]);
                        double val = Double.parseDouble(values[2]);
                        return new Tuple3(id, ts, val);
                    }
                });

        // Aggregate to timestamp
        JavaRDD<String> result = x.mapToPair(
            new PairFunction<Tuple3<Integer, Integer, Double>, Tuple2<Integer, Integer>, Tuple2<Double, Integer>>() {
                @Override
                public Tuple2<Tuple2<Integer, Integer>, Tuple2<Double, Integer>> call(
                        Tuple3<Integer, Integer, Double> t)
                        throws Exception {
                    return new Tuple2<Tuple2<Integer, Integer>, Tuple2<Double, Integer>>(
                            new Tuple2<Integer, Integer>(t._1(), t
                                    ._2()), new Tuple2<Double, Integer>(t._3(), 1));
                }
                // Aggregate values
            }).reduceByKey(new Function2<Tuple2<Double, Integer>, Tuple2<Double, Integer>, Tuple2<Double, Integer>>() {
                @Override
                public Tuple2<Double, Integer> call(Tuple2<Double, Integer> v1, Tuple2<Double, Integer> v2)
                        throws Exception {
                    return new Tuple2<Double, Integer>(v1._1() + v2._1(), v1._2() + v2._2());
                }
                // Average of values and import to csv like format
        }).map(new Function<Tuple2<Tuple2<Integer, Integer>, Tuple2<Double, Integer>>, String>() {
            @Override
            public String call(Tuple2<Tuple2<Integer, Integer>, Tuple2<Double, Integer>> v)
                    throws Exception {
                return String.format("%s,%s,%s", v._1._1, v._1._2, (double)v._2._1/v._2._2 );
            }
        });

        // Save to hadoop
        result.saveAsTextFile(".gitignoreresult");
    }
}