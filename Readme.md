## Scripts

* start_hadoop_nodes.sh (start hadoop nodes and yarn);
* start.sh (start mar reduce work on input data and write into output dir hdfs).
* del_output_hdfs.sh (remove old output artifacts)
* scripts/start_test.sh (start unit test for mapper and reducer)
* hdfs dfs -cat result/* (watch the result of work map-reduce)
* scripts/cassandra_api.py Create keyspace, table, and view result in cassandra


## Problem
Spark worker for aggregae metics form id and timestamp